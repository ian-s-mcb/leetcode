/**
 * Runtime: 79 ms, faster than 65.51% of JavaScript online submissions
 * Memory Usage: 42.2 MB, less than 33.41% of JavaScript online submissions
 * Date: 2022-07-11
 *
 * Based on:
 *   * My initial solution
 *
 * @param {string} haystack
 * @param {string} needle
 * @return {number}
*/
// Brute-Force - O(n * m)
// where n = length of haystack, m = length of needle
export default function strStr (haystack, needle) {
  if (!needle) { return 0 }
  let i = 0
  let j = 0
  while (i < haystack.length) {
    if (haystack[i] === needle[j]) {
      j++
      i++
      if (j === needle.length) { return i - needle.length }
    } else {
      i = i - j + 1
      j = 0
    }
  }
  return -1
}
