/**
 * @param {number} x
 * @return {boolean}
 */
export default function isPalindrome (x) {
  let tmp = Math.abs(x)
  let d
  let reverse = 0
  while (tmp != 0) {
    d = tmp % 10
    tmp = Math.floor(tmp / 10)
    reverse = reverse * 10 + d
  }
  return x === reverse
};
