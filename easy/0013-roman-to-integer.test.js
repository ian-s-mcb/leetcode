import romanToInt from './0013-roman-to-integer'

test('handles a numeral with a single digit: I', () => {
  expect(romanToInt('I')).toBe(1)
})

test('handles a numeral with a single digit: X', () => {
  expect(romanToInt('X')).toBe(10)
})

test('handles a numeral with two accumulating digits: II', () => {
  expect(romanToInt('II')).toBe(2)
})

test('handles a numeral with two accumulating digits: XX', () => {
  expect(romanToInt('XX')).toBe(20)
})

test('handles a numeral with a decrementing digit: IX', () => {
  expect(romanToInt('IX')).toBe(9)
})

test('handles a numeral with both incrementing and decrementing digits: XIX', () => {
  expect(romanToInt('XIX')).toBe(19)
})
