import longestCommonPrefix from './0014-longest-common-prefix'

test('handles positive case with n=3 strings with ans=2 characters', () => {
  const strs = ['flower', 'flow', 'flight']
  expect(longestCommonPrefix(strs)).toBe('fl')
})

test('handles negative case with n=3 strings', () => {
  const strs = ['dog', 'racecar', 'car']
  expect(longestCommonPrefix(strs)).toBe('')
})

test('handles positive case with n=10 ordered strings with ans=6 characters', () => {
  const strs = ['flower', 'flowera', 'flowerb', 'flowerc', 'flowerd', 'flowere', 'flowerf', 'flowerg', 'flowerh', 'floweri']
  expect(longestCommonPrefix(strs)).toBe('flower')
})

test('handles positive case with n=10 scrambled strings with ans=6 characters', () => {
  const strs = ['flowerbbbb', 'floweraaa',  'flowerc', 'flowerd', 'flower', 'floweree', 'flowergggg', 'flowerfffff', 'flowerh', 'flowerii']
  expect(longestCommonPrefix(strs)).toBe('flower')
})

test('handles positive case with n=10 ordered strings with ans=1 character', () => {
  const strs = ['flower', 'flowera', 'flowerb', 'flowerc', 'flowerd', 'f', 'flowerf', 'flowerg', 'flowerh', 'floweri']
  expect(longestCommonPrefix(strs)).toBe('f')
})
