/**
 * Runtime: 87 ms, faster than 54.43% of JavaScript online submissions
 * Memory Usage: 42.7 MB, less than 19.19% of JavaScript online submissions
 * Date: 2022-07-02
 *
 * Based on:
 *   * My initial solution
 *
 * @param {string} s
 * @return {boolean}
 */
export default function isValid (s) {
  let stack = []
  let pair = {
    ')': '(',
    ']': '[',
    '}': '{',
  }
  if (s.length === 0) { return true }
  for (let i = 0; i < s.length; i++) {
    switch (s[i]) {
      case '(':
      case '[':
      case '{':
        stack.push(s[i])
        break
      case ')':
      case ']':
      case '}':
        if ((!stack.length) || stack.pop() !== pair[s[i]]) { return false }
        break
      default:
        throw new Error('Invalid character in input string')
        break
    }// end switch
  }
  return stack.length === 0
}
