import isValid from './0020-valid-parentheses'

test('solves empty string case', () => {
  expect(isValid('')).toEqual(true)
})

test('solves smallest valid case', () => {
  expect(isValid('()')).toEqual(true)
  expect(isValid('[]')).toEqual(true)
  expect(isValid('{}')).toEqual(true)
})

test('solves two smallest invalid cases', () => {
  expect(isValid('(')).toEqual(false)
  expect(isValid(')')).toEqual(false)

  expect(isValid('[')).toEqual(false)
  expect(isValid(']')).toEqual(false)

  expect(isValid('{')).toEqual(false)
  expect(isValid('}')).toEqual(false)
})

test('solves valid case with three serial pairs', () => {
  expect(isValid('()()()')).toEqual(true)
  expect(isValid('[][][]')).toEqual(true)
  expect(isValid('{}{}{}')).toEqual(true)
})

test('solves valid case with three nested pairs', () => {
  expect(isValid('((()))')).toEqual(true)
  expect(isValid('[[[]]]')).toEqual(true)
  expect(isValid('{{{}}}')).toEqual(true)
})

test('solves two small invalid cases', () => {
  expect(isValid('(()')).toEqual(false)
  expect(isValid('[[]')).toEqual(false)
  expect(isValid('{{}')).toEqual(false)

  expect(isValid('())')).toEqual(false)
  expect(isValid('[]]')).toEqual(false)
  expect(isValid('{}}')).toEqual(false)
})

test('solves invalid cases with mismatched close parentheses', () => {
  expect(isValid('(]')).toEqual(false)
  expect(isValid('(}')).toEqual(false)
  expect(isValid('[)')).toEqual(false)
  expect(isValid('[}')).toEqual(false)
  expect(isValid('{)')).toEqual(false)
  expect(isValid('{]')).toEqual(false)
})
