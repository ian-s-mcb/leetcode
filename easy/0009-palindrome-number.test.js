import isPalidrome from './0009-palindrome-number'

test("handles positive check with an input that's 3 digits long", () => {
  let x = 121
  expect(isPalidrome(x)).toBeTruthy()
})

test("handles negative check with an input that's 3 digits long", () => {
  let x = 122
  expect(isPalidrome(x)).toBeFalsy()
})

test("handles positive check with an input that's 6 digits long", () => {
  let x = 123321
  expect(isPalidrome(x)).toBeTruthy()
})

test("handles negative check with an input that's 6 digits long", () => {
  let x = 123421
  expect(isPalidrome(x)).toBeFalsy()
})

test("handles negative check with an input that's negative", () => {
  let x = -121
  expect(isPalidrome(x)).toBeFalsy()
})
