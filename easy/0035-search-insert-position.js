/**
 * Runtime: 47 ms, faster than 99.87% of JavaScript online submissions
 * Memory Usage: 41.8 MB, less than 88.51% of JavaScript online submissions
 * Date: 2022-07-13
 *
 * Based on:
 *   * My initial solution
*/

// Attempt-3 - Tweak reduction of righthand side:
//             r = m vs. r = m - 1.
//             Both give correct answer, but former is more appropriate
//             since there is nothing to the left of middle when array /
//             array portion length is 2 given that middle is calculated
//             as the exact middle or a leftish middle.
export default function searchInsert (nums, target) {
  let l = 0
  let r = nums.length - 1
  while (l < r) {
    let m = Math.floor((l + r) / 2)
    if (nums[m] === target) {
      return m
    }
    else if (nums[m] > target) {
      r = m
    } else {
      l = m + 1
    }
  }
  return nums[l] < target ? l + 1 : l
}

// Attempt-2 - Fancy use of alternative initial r value
//             Came from Leetcode discussion
// export default function searchInsert (nums, target) {
//   let l = 0
//   let r = nums.length
//   let m
//   while (l < r) {
//     m = Math.floor((l + r) / 2)
//     if (target <= nums[m]) {
//       r = m
//     } else {
//       l = m + 1
//     }
//   }
//   return l
// }

// Attempt-1
// export default function searchInsert (nums, target) {
//   let l = 0
//   let r = nums.length - 1
//   let m
//   while (l < r) {
//     m = Math.floor((l + r) / 2)
//     if (nums[m] < target) {
//       l = m + 1
//     } else if (nums[m] > target) {
//       r = m - 1
//     } else {
//       return m
//     }
//   }
//   return nums[l] < target ? l + 1 : l
// }
