/**
 * Runtime: 71 ms, faster than 80.58% of JavaScript online submissions
 * Memory Usage: 42.2 MB, less than 33.51% of JavaScript online submissions
 * Date: 2022-07-09
 *
 * Based on:
 *   * My initial solution
 *
 * @param {number[]} nums
 * @param {number} val
 * @return {number}
*/
export default function removeElement (nums, val) {
  let k = nums.length
  for (let i = 0, j = 0; i < nums.length; i++) {
    if (nums[i] === val) {
      k--
    } else {
      nums[j] = nums[i]
      j++
    }
  }
  return k
}
