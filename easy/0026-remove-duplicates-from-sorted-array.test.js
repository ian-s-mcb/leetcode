import removeDuplicates from './0026-remove-duplicates-from-sorted-array'

test('solves when no duplicates exist', () => {
  const nums = [1, 2, 3]
  const expectedNums = [1, 2, 3]
  const expectedK = 3
  expect(removeDuplicates(nums)).toEqual(expectedK)
  expect(nums).toEqual(expectedNums)
})

test('solves when dup at beginning of array of length 3', () => {
  const nums = [1, 1, 2]
  const expectedNums = [1, 2]
  const expectedK = 2
  expect(removeDuplicates(nums)).toEqual(expectedK)
  expect(nums.slice(0, expectedK)).toEqual(expectedNums)
})

test('solves when dup at beginning of array of length 4', () => {
  const nums = [1, 1, 2, 3]
  const expectedNums = [1, 2, 3]
  const expectedK = 3
  const dedupedLength = nums.length - expectedK
  expect(removeDuplicates(nums)).toEqual(expectedK)
  expect(nums.slice(0, expectedK)).toEqual(expectedNums)
})

test('solves when dup at end', () => {
  const nums = [1, 2, 3, 3, 3]
  const expectedNums = [1, 2, 3]
  const expectedK = 3
  const dedupedLength = nums.length - expectedK
  expect(removeDuplicates(nums)).toEqual(expectedK)
  expect(nums.slice(0, expectedK)).toEqual(expectedNums)
})

test('solves when dup at middle', () => {
  const nums = [1, 2, 2, 2, 2, 3]
  const expectedNums = [1, 2, 3]
  const expectedK = 3
  const dedupedLength = nums.length - expectedK
  expect(removeDuplicates(nums)).toEqual(expectedK)
  expect(nums.slice(0, expectedK)).toEqual(expectedNums)
})

test('solves when scattered dups', () => {
  const nums = [1, 2, 2, 3, 4, 4, 5, 5, 6, 6, 6]
  const expectedNums = [1, 2, 3, 4, 5, 6]
  const expectedK = 6
  const dedupedLength = nums.length - expectedK
  expect(removeDuplicates(nums)).toEqual(expectedK)
  expect(nums.slice(0, expectedK)).toEqual(expectedNums)
})
