import strStr from './0028-implement-strstr'

describe('corner case', () => {
  test('handles when needle is empty string', () => {
    let haystack = 'aab'
    let needle = ''
    let ans = 0
    expect(strStr(haystack, needle)).toBe(ans)
  })
  test('handles when needle is longer than haystack', () => {
    let haystack = 'aabb'
    let needle = 'aabba'
    let ans = -1
    expect(strStr(haystack, needle)).toBe(ans)
  })
})

describe("cases where needle isn't present", () => {
  test('handles when needle length 1', () => {
    let haystack = 'aab'
    let needle = 'c'
    let ans = -1
    expect(strStr(haystack, needle)).toBe(ans)
  })

  test('handles when needle length 2 and partial match', () => {
    let haystack = 'aab'
    let needle = 'ac'
    let ans = -1
    expect(strStr(haystack, needle)).toBe(ans)
  })

  test('handles when needle length 2 and total mismatch', () => {
    let haystack = 'aab'
    let needle = 'xx'
    let ans = -1
    expect(strStr(haystack, needle)).toBe(ans)
  })
})

describe("cases where needle is present", () => {
  test('handles when needle of length 1 is at front', () => {
    let haystack = 'aab'
    let needle = 'a'
    let ans = 0
    expect(strStr(haystack, needle)).toBe(ans)
  })

  test('handles when needle of length 1 is at rear', () => {
    let haystack = 'aab'
    let needle = 'b'
    let ans = 2
    expect(strStr(haystack, needle)).toBe(ans)
  })

  test('handles when needle of length 1 is at middle', () => {
    let haystack = 'aabcd'
    let needle = 'b'
    let ans = 2
    expect(strStr(haystack, needle)).toBe(ans)
  })

  test('handles when needle of length 2 is at front', () => {
    let haystack = 'aabb'
    let needle = 'aa'
    let ans = 0
    expect(strStr(haystack, needle)).toBe(ans)
  })

  test('handles when needle of length 2 is at rear', () => {
    let haystack = 'aabb'
    let needle = 'bb'
    let ans = 2
    expect(strStr(haystack, needle)).toBe(ans)
  })

  test('handles when needle of length 2 is at middle', () => {
    let haystack = 'aabbcd'
    let needle = 'bb'
    let ans = 2
    expect(strStr(haystack, needle)).toBe(ans)
  })
})

describe('tricky cases', () => {
  test('handles when partial match overlaps with actual match', () => {
    let haystack = 'aabba'
    let needle = 'ba'
    let ans = 3
    expect(strStr(haystack, needle)).toBe(ans)
  })

  test('handles when longer overlap', () => {
    let haystack = 'mississippi'
    let needle = 'issip'
    let ans = 4
    expect(strStr(haystack, needle)).toBe(ans)
  })
})
