import twoSum from './0001-two-sum'

test('solves small case (n=3)', () => {
  let nums = [5, 3, 4]
  let target = 9
  expect(twoSum(nums, target)).toEqual([0, 2])
})
