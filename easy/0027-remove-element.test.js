import removeElement from './0027-remove-element'

test('solves array without target val', () => {
  let nums = [1, 2, 3]
  let target = 5
  let expectedNums = [...nums]
  let k = nums.length
  expect(removeElement(nums, target)).toEqual(k)
  expect(nums).toEqual(expectedNums)
})

test('solves array of length 1 containing target val', () => {
  let nums = [1]
  let target = 1
  let expectedNums = []
  let k = nums.length - 1
  expect(removeElement(nums, target)).toEqual(k)
  expect(nums.slice(0, k)).toEqual(expectedNums)
})

test('solves array of length 2 with target val at front', () => {
  let nums = [1, 2]
  let target = 1
  let expectedNums = [2]
  let k = nums.length - 1
  expect(removeElement(nums, target)).toEqual(k)
  expect(nums.slice(0, k)).toEqual(expectedNums)
})

test('solves array of length 2 with target val at rear', () => {
  let nums = [1, 2]
  let target = 2
  let expectedNums = [1]
  let k = nums.length - 1
  expect(removeElement(nums, target)).toEqual(k)
  expect(nums.slice(0, k)).toEqual(expectedNums)
})

test('solves array of length 3 with target val at middle', () => {
  let nums = [1, 2, 3]
  let target = 2
  let expectedNums = [1, 3]
  let k = nums.length - 1
  expect(removeElement(nums, target)).toEqual(k)
  expect(nums.slice(0, k)).toEqual(expectedNums)
})

test('solves array of length 3 with two occurrences of target val at front', () => {
  let nums = [1, 1, 2]
  let target = 1
  let expectedNums = [2]
  let k = nums.length - 2
  expect(removeElement(nums, target)).toEqual(k)
  expect(nums.slice(0, k)).toEqual(expectedNums)
})

test('solves array of length 3 with two occurrences of target val at rear', () => {
  let nums = [1, 2, 2]
  let target = 2
  let expectedNums = [1]
  let k = nums.length - 2
  expect(removeElement(nums, target)).toEqual(k)
  expect(nums.slice(0, k)).toEqual(expectedNums)
})
