import searchInsert from './0035-search-insert-position'

describe('corner cases', () => {
  test('handles empty array', () => {
    let nums = []
    let target = 1
    expect(searchInsert(nums, target)).toBe(0)
  })
})

describe('cases where target exists', () => {
  test('handles array length 1', () => {
    let nums = [1]
    let target = 1
    expect(searchInsert(nums, target)).toBe(0)
  })

  test('handles array length 3', () => {
    let nums = [1, 2, 3]
    let target = 1
    expect(searchInsert(nums, target)).toBe(0)
  })

  test('handles array length 3', () => {
    let nums = [1, 2, 3]
    let target = 2
    expect(searchInsert(nums, target)).toBe(1)
  })

  test('handles array length 3', () => {
    let nums = [1, 2, 3]
    let target = 3
    expect(searchInsert(nums, target)).toBe(2)
  })

  test('handles array length 9', () => {
    let nums = [1, 2, 3, 4, 5, 6, 8, 9]
    let target = 5
    expect(searchInsert(nums, target)).toBe(4)
  })
})

describe("cases where target doesn't exist", () => {
  test('handles array length 1', () => {
    let nums = [1]
    let target = 2
    expect(searchInsert(nums, target)).toBe(1)
  })

  test('handles array length 2', () => {
    let nums = [1, 3]
    let target = 2
    expect(searchInsert(nums, target)).toBe(1)
  })

  test('handles array length 2', () => {
    let nums = [1, 3]
    let target = 0
    expect(searchInsert(nums, target)).toBe(0)
  })

  test('handles array length 3', () => {
    let nums = [1, 3, 5]
    let target = 4
    expect(searchInsert(nums, target)).toBe(2)
  })

  test('handles array length 4', () => {
    let nums = [1, 5, 7, 9]
    let target = 6
    expect(searchInsert(nums, target)).toBe(2)
  })

  test('handles array length 9', () => {
    let nums = [0, 1, 3, 4, 5, 6, 8, 9]
    let target = 2
    expect(searchInsert(nums, target)).toBe(2)
  })
})
