import mergeTwoLists, { ListNode } from './0021-merge-two-sorted-lists'

test('solves case where l2 belongs entirely after l1', () => {
  const l1 = new ListNode(1, new ListNode(3, new ListNode(5)))
  const l2 = new ListNode(6, new ListNode(7, new ListNode(8)))
  const ans = new ListNode(1, new ListNode(3, new ListNode(5, new ListNode(6, new ListNode(7, new ListNode(8))))))

  expect(mergeTwoLists(l1, l2)).toEqual(ans)
})

test('solves case where l1 belongs entirely after l2', () => {
  const l1 = new ListNode(6, new ListNode(7, new ListNode(8)))
  const l2 = new ListNode(1, new ListNode(3, new ListNode(5)))
  const ans = new ListNode(1, new ListNode(3, new ListNode(5, new ListNode(6, new ListNode(7, new ListNode(8))))))

  expect(mergeTwoLists(l1, l2)).toEqual(ans)
})

test('solves case l1 belongs partially before l2', () => {
  const l1 = new ListNode(4, new ListNode(7))
  const l2 = new ListNode(1, new ListNode(5))
  const ans = new ListNode(1, new ListNode(4, new ListNode(5, new ListNode(7))))

  expect(mergeTwoLists(l1, l2)).toEqual(ans)
})
