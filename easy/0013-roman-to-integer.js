/**
 * Runtime: 204 ms, faster than 36.72% of JavaScript online submissions
 * Memory Usage: 46.7 MB, less than 82.43% of JavaScript online submissions
 * Date: 2022-05-06
 * Based on:
 *   * User: user9697N
 *   * Post: https://leetcode.com/problems/roman-to-integer/discuss/373635/JavaScript%3A-runtime-95
 *
 * @param {string} s
 * @return {number}
*/
export default function romanToInt(s) {
  let hash = {I: 1, V: 5, X: 10, L: 50, C: 100, D: 500, M: 1000}
  let ans = 0, cur = 0, prev = 0, j = s.length - 1
  for(; j >= 0; j -= 1){
    cur = hash[s[j]]
    cur < prev ? ans -= cur : ans += cur
    prev = cur
  }
  return ans 
}
