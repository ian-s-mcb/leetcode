/**
 * Runtime: 79 ms, faster than 80.95% of JavaScript online submissions
 * Memory Usage: 44 MB, less than 78.75% of JavaScript online submissions
 * Date: 2022-07-02
 *
 * Based on:
 *   * My initial solution
 *
 * @param {ListNode} list1
 * @param {ListNode} list2
 * @return {ListNode}
*/
export function ListNode (val, next) {
  this.val = (val===undefined ? 0 : val)
  this.next = (next===undefined ? null : next)
}
export default function mergeTwoLists (list1, list2) {
  if (!list1) return list2
  else if (!list2) return list1
  else if (list1.val < list2.val) {
    return new ListNode(list1.val, mergeTwoLists(list1.next, list2))
  } else {
    return new ListNode(list2.val, mergeTwoLists(list1, list2.next))
  }
}
