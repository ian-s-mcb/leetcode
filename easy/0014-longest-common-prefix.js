/**
 * Runtime: 62 ms, faster than 93.37% of JavaScript online submissions
 * Memory Usage: 42.6 MB, less than 55.29% of JavaScript online submissions
 * Date: 2022-05-06
 *
 * Based on:
 *   * My initial solution and small improvements from official,
 *   "horizontal scanning" solution
 *
 * @param {string[]} strs
 * @return {string}
 */

/*
// Vertical scanning
export default function longestCommonPrefix (strs) {
  let prefix = strs[0]
  for (let si = 1; si < strs.length; si++) {
    while (strs[si].indexOf(prefix) !== 0) {
      prefix = prefix.substr(0, prefix.length - 1)
      if (!prefix) { return '' }
    }
  }
  return prefix
}
*/

// Horizonal scanning
export default function longestCommonPrefix (strs) {
  let ans = '', cur
  for (let ci = 0; ci < strs[0].length; ci++) {
    cur = strs[0][ci]
    for (let si = 1; si < strs.length; si++) {
      if (strs[si][ci] !== cur) {
        return ans
      }
    }
    ans += cur
  }
  return strs[0]
}
