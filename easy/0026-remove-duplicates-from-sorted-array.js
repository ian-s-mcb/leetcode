/**
 * Runtime: 110 ms, faster than 61.78% of JavaScript online submissions
 * Memory Usage: 44.5 MB, less than 69.22% of JavaScript online submissions
 * Date: 2022-07-02
 *
 * Based on:
 *   * A simplication on my initial solution
 *
 * @param {number[]} nums
 * @return {number}
*/
export default function removeDuplicates (nums) {
  if (!nums.length) { return 0 }
  let k = nums.length
  let i, j;
  for (i = 1, j = 0; i < nums.length; i++) {
    if (nums[i - 1] === nums[i]) {
      k--
    } else {
      nums[j] = nums[i - 1]
      j++
    }
  }
  nums[j] = nums[i - 1]
  return k
}
